# Kylin Java Pack(Support OpenJDK11)
- Fork of vscjava.vscode-java-pack, modified on the lower version to support OpenJDK 11
- Modified based on the version 0.25.2 of vscjava.vscode-java-pack
- A  collection of popular extensions that can help write, test and debug Java applications in VSCode or kylin-ide.It is a extensions package to download extensions:[Kylin Java(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/java),[Kylin Java Debug(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-java-debug),[Kylin Java Test(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-java-test),[Kylin Java Maven(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-maven) and [Kylin Java Dependency(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-java-dependency)
## Extensions Included

By installing Extension Pack for Java, the following extensions are installed:

- [Kylin Java(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/java)
    - Code Navigation
    - Auto Completion
    - Refactoring
    - Code Snippets
- [Kylin Java Debug(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-java-debug)
    - Debugging
- [Kylin Java Test(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-java-test)
    - Run & Debug JUnit/TestNG Test Cases
- [Kylin Java Maven(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-maven)
    - Project Scaffolding
    - Custom Goals
- [Kylin Java Dependency(Support OpenJDK11)](https://gitee.com/openkylin/extensions-repo/tree/master/KylinIDETeam/vscode-java-dependency)
    - Manage Java projects, referenced libraries, resource files, packages, classes, and class members


